﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WebServiceHW
{
   
    public partial class HW7File
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class HW7File
    {
        public static HW7File[] FromJson(string json) => JsonConvert.DeserializeObject<HW7File[]>(json, WebServiceHW.Converter.Settings);

    }

    public static class Serialize
    {
        public static string ToJson(this HW7File[] self) => JsonConvert.SerializeObject(self, WebServiceHW.Converter.Settings);

    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

}
