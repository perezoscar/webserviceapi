﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace WebServiceHW
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

 
    void Handle_CheckNetwork(object sender, System.EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected == true)
            {
                ConnectionStatusLabel.Text = "Connected to Internet" +
                    CrossConnectivity.Current.ConnectionTypes.First();
            }
            else
            {
                ConnectionStatusLabel.Text = "No Internet Connection";
            }
        }

        async void Handle_GetDefinition(object sender, System.EventArgs e)
        {
            Type.Text = "Type: ";
            Definition.Text = "Definition: ";
            Example.Text = "Example: ";

            if(CrossConnectivity.Current.IsConnected == false)
            {
                await DisplayAlert("Error", "No internet Conection", "Ok");
            }

            else
            {
                HttpClient client = new HttpClient();
                var uri = new Uri
                    (string.Format($"https://owlbot.info/api/v2/dictionary/" + $"{WordSearch.Text.ToLower()}"));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                HttpResponseMessage response = await client.SendAsync(request);
                HW7File[] DictionaryApiData = null;

                if(response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    if(content =="[]")
                    {
                        await DisplayAlert("Error", "Not a word", "OK");
                    }
                    else
                    {
                        DictionaryApiData = HW7File.FromJson(content);
                        var type = DictionaryApiData[0].Type;
                        Type.Text += type;
                        var word = DictionaryApiData[0].Definition;
                        Definition.Text += word;
                        var example = DictionaryApiData[0].Example;
                        Example.Text += example;
                    }
                }
            }
        }
    }
}
